variable "prefix" {
    description = "The defauld name prefix for Azure resources"
    default     = "varonis"
}

variable "location_east"  {
    description = "The uniqe location for Azure resources to exist"
    default     = "eastus"
}

variable "location_west"  {
    description = "The uniqe location for Azure resources to exist"
    default     = "westus"
}