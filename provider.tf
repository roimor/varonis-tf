provider "azurerm" {
  version = "=2.5.0"
  features {}

  // Authentication was provided by environment variables in local machine and $ az login --service-principal -u CLIENT_ID -p CLIENT_SECRET --tenant TENANT_ID 
  // Or another option, providing auths in the provider block:

  # subscription_id = "..."
  # client_id       = "..."
  # client_secret   = "..."
  # tenant_id       = "..."
}