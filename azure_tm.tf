module "east" {
  source   = "./modules/vms"
  prefix   = "${var.prefix}"
  location = "${var.location_east}" 
}

module "west" {
  source   = "./modules/vms"
  prefix   = "${var.prefix}"
  location = "${var.location_west}" 
}

resource "azurerm_resource_group" "traffic" {
  name     = "${var.prefix}-traffic"
  location = "${var.location_east}"
}

# Create Traffic Manager API Profile
resource "azurerm_traffic_manager_profile" "main" {
  name                   = "${var.prefix}-tmprofile"
  resource_group_name    = azurerm_resource_group.traffic.name
  traffic_routing_method = "Geographic"

  dns_config {
    relative_name = "varonis"
    ttl           = 30
  }

  monitor_config {
    protocol = "http"
    port     = 80
    path     = "/"
  }
}

# Create Traffic Manager - East End Point
resource "azurerm_traffic_manager_endpoint" "east" {
  name                = "${var.prefix}-east"
  resource_group_name = azurerm_resource_group.traffic.name
  profile_name        = "${azurerm_traffic_manager_profile.main.name}"
  target              = "10.0.2.5"
  type                = "externalEndpoints"
  geo_mappings        = ["WORLD"]
}

# Create Traffic Manager - West End Point
resource "azurerm_traffic_manager_endpoint" "west" {
  name                = "${var.prefix}-west"
  resource_group_name = azurerm_resource_group.traffic.name
  profile_name        = "${azurerm_traffic_manager_profile.main.name}"
  target              = "10.0.2.4"
  type                = "externalEndpoints"
  geo_mappings        = ["US"]
}