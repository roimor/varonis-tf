# Provisioning Azure Infrastructure via Terraform

### A Terraform based project for Azure infrastructure resources provisioning, using the [azurerm](https://registry.terraform.io/providers/hashicorp/azurerm/latest) provider.


# Description

Using this repo you'll be able to quickly deploy Cloud resources within the Azure echosystem in two regions.
For this assignment we will use "varonis" as the main resources prefix name. 

* This project stack will consist of few components:
    - Azure virtual networks.
    - Azure virtual machines.
    - Azure load balancers.
    - Azure traffic manager, using the load balancers as endpoints.

## Getting Started

### Requirements

* Terraform ~>v0.12.5
* Provider azurerm ~>v2.5.0

### Installing

* Clone this Repo
```
$ git clone https://roimor@bitbucket.org/roimor/varonis-tf.git
$ cd varonis-tf
```

*  Apply the changes to reach the desired state of the configuration. You should see - "Apply complete!"

```
$ terraform plan
$ terraform apply -auto-approve
```

* Don't forget to cleanup your workspace :-)

```
$ terraform destroy
```

