variable "prefix" {
  description = "The prefix used for all Azure resources"
}

variable "location" {
    description = "The location where Azure resources should exist"
}