output "load_balancer_id" {
  value = "${azurerm_lb.main.id}"
}

output "subnet_id" {
  value = "${azurerm_subnet.internal.id}"
}